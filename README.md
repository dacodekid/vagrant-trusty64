# Trusty64/Vagrant/Docker/Compose

### Base Vagrantfile that...
- Based on Ubuntu/Trusty64 official image

- Installs the following **vagrant plugins** if not installed
  - [vagrant-digitalocean] (https://github.com/devopsgroup-io/vagrant-digitalocean)
  - [vagrant-env] (https://github.com/gosuri/vagrant-env)
  - [vagrant-docker-compose] (https://github.com/leighmcculloch/vagrant-docker-compose)

- Installs the following **provisioners**
  - Docker
  - Docker-Compose

- With initial **shell** provision, Installs
  - a 2GB swap file
  - git
  - zsh shell
  - oh-my-zsh
  - change zsh default theme
  - reads **id_rsa.pub** file and inserts into *root/.ssh/authorized_keys* file (so you can `vagrant ssh` as root without password)

### Create a custom box based off of this:
- `git clone https://github.com/dacodekid/vagrant-trusty64.git`
- `cd vagrant-trusty64`
- `vagrant up`
- `vagrant package default --output ~/.vagrant.d/boxes/vagrant-trusty64.box`
- `cd ~/.vagrant.d/boxes`
- `vagrant box add vagrant-trusty64 vagrant-trusty64.box`

**That's it**. Now we can refer this box as
`config.vm.box = "vagrant-trusty64"`
