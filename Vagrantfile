# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  # *******************************************
  # ***************** PLUGINS *****************
  # *******************************************

  # Check for missing plugins
  required_plugins = %w(vagrant-digitalocean vagrant-env vagrant-docker-compose)
  plugin_installed = false
  required_plugins.each do |plugin|
    unless Vagrant.has_plugin?(plugin)
      system "vagrant plugin install #{plugin}"
      plugin_installed = true
    end
  end

  # If new plugins installed, restart Vagrant process
  if plugin_installed === true
    exec "vagrant #{ARGV.join' '}"
  end


  # *******************************************
  # ************* BASE MACHINE ****************
  # *******************************************

  config.vm.box = "ubuntu/trusty64"
  config.vm.provision "shell" do |shell|
    ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
    shell.inline = <<-SHELL
      # size of swapfile in megabytes
      swapsize=2048

      # does the swap file already exist?
      grep -q "swapfile" /etc/fstab

      # if not then create it
      if [ $? -ne 0 ]; then
      	echo 'swapfile not found. Adding swapfile.'
      	fallocate -l ${swapsize}M /swapfile
      	chmod 600 /swapfile
      	mkswap /swapfile
      	swapon /swapfile
      	echo '/swapfile none swap defaults 0 0' >> /etc/fstab
      else
      	echo 'swapfile found. No changes made.'
      fi

      # output results to terminal
      cat /proc/swaps
      cat /proc/meminfo | grep Swap

      # usual stuff
      apt-get update -y
      apt-get install git git-core zsh -y
      sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
      sed -i 's/robbyrussell/bira/g' ~/.zshrc
      echo #{ssh_pub_key} > /root/.ssh/authorized_keys
    SHELL
  end

  config.vm.provision :docker
  config.vm.provision :docker_compose
end
